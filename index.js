const fs = require("fs");
const crypto = require("crypto");
const http = require("http");
const https = require("https");

let clients = {};

http.createServer((req, res) => {
    console.log(req.url)
    if(req.url === "/index") {
        res.writeHead(200, {
            "Set-Cookie": crypto.randomBytes(20).toString("base64") 
        })
        fs.createReadStream("./index.html").pipe(res);
    }
    else if(req.url === "/events") {
        if(req.headers.cookie) {
            clients[req.headers.cookie] = res;
        }

        res.writeHead(200, {
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive',
            'Cache-Control': 'no-cache'
        });

        let i = setInterval(() => {
            res.write("event: add\n");
            res.write("data: add....\n\n");
        }, 5000);

        setInterval(() => {
            const data = `data: ${new Date().toISOString()}\n\n`;
            res.write("event: data\n");
            res.write(data);
        }, 1000);


        // setInterval(() => {
        //     https.get(`https://source.unsplash.com/random`, (r) => {
        //         console.log(r.headers.location);
        //         if(r.headers.location.match(/^https/)) {
        //             console.log(`Download image from ${r.headers.location}`)
        //             https.get(r.headers.location, (hs) => {
        //                 let d = ""
        //                 hs.on("data", (chunk) => { 
        //                     d += chunk.toString("base64")
        //                 })
        //                 hs.on("end", () => { 
        //                     res.write("event: image\n");
        //                     res.write(`data: ${d}\n\n`)                        
        //                 });
        //             })
        //         }
        //     })
        // }, 10000);


        req.on("close", () => clearInterval(i))
    }
    else if(req.url === "/image") {
        if(clients[req.headers.cookie]) {
            https.get(`https://source.unsplash.com/1600x900/?singapore`, (r) => {
                console.log(r.headers.location);
                if(r.headers.location.match(/^https/)) {
                    console.log(`Download image from ${r.headers.location}`)
                    https.get(r.headers.location, (hs) => {
                        let d = ""
                        hs.on("data", (chunk) => { 
                            d += chunk.toString("base64")
                        })
                        hs.on("end", () => { 
                            clients[req.headers.cookie].write("event: image\n");
                            clients[req.headers.cookie].write(`data: ${d}\n\n`)                        
                        });
                    })
                }
            })
        }

        res.end();
    }
    
}).listen(8888);